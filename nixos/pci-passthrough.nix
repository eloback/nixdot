{config, pkgs, ...}:
{
  boot.kernelParams = [ "amd_iommu=on" ];

  boot.kernelModules = [ "amdgpu" "vfio" "vfio_iommu_type1" "vfio_pci" "vfio_virqfd" ];

#  services.xserver.videoDrivers = [ "amdgpu" ];
#  services.wayland.videoDrivers = [ "amdgpu" ];

  boot.extraModprobeConfig =''
    options vfio-pci ids=10de:2482,10de:228b
    softdep drm pre: vfio-pci
  '';

systemd.services.libvirtd.preStart = let
    qemuHook = pkgs.writeScript "qemu-hook" ''
      #!/bin/sh

      command=$2

      if [ "$command" = "started" ]; then
        systemctl set-property --runtime -- system.slice AllowedCPUs=0-2,6-8
        systemctl set-property --runtime -- user.slice AllowedCPUs=0-2,6-8
        systemctl set-property --runtime -- init.scope AllowedCPUs=0-2,6-8
      elif [ "$command" = "release" ]; then
        systemctl set-property --runtime -- system.slice AllowedCPUs=0-2,6-8
        systemctl set-property --runtime -- user.slice AllowedCPUs=0-2,6-8
        systemctl set-property --runtime -- init.scope AllowedCPUs=0-2,6-8
      fi
    '';
  in ''
    mkdir -p /var/lib/libvirt/hooks
    chmod 755 /var/lib/libvirt/hooks

    # Copy hook files
    ln -sf ${qemuHook} /var/lib/libvirt/hooks/qemu
  '';

  virtualisation = {
    libvirtd = {
      enable = true;
      extraConfig = ''
       user="eloback"
      '';


      qemu = {
        verbatimConfig = ''
          namespaces = []
          user= "+1000"
          nvram = [ "${pkgs.OVMF}/FV/OVMF.fd:${pkgs.OVMF}/FV/OVMF_VARS.fd" ]
        '';
      };
    };
    spiceUSBRedirection.enable = true;
  };

  users.groups.libvirtd.members = [ "root" "eloback" ];


  environment.systemPackages = with pkgs; [
    htop
    geekbench
    hwloc
    pciutils
    virtmanager
    qemu
    OVMF
  ];

  systemd.tmpfiles.rules = [
    "f /dev/shm/looking-glass 0660 eloback qemu-libvirtd -"
  ];
}
