{ config, pkgs, ... }:

{
  wayland.windowManager.sway = {
    enable = true;
    config = rec {
      modifier = "Mod4";
      startup = [
        # Launch Firefox on start
        {command = "firefox";}
      ];
    };
  };
}
