{ pkgs, ... }:
{
  nixpkgs = {
    config = {
      allowUnfree = true;
      allowUnfreePredicate = (_: true);
    };
  };

  programs.gpg.enable = true;
  services.gpg-agent = {
    enable = true;
    enableFishIntegration = true;
    enableSshSupport = true;
  };

  # programs.neovim = {
  #   enable = true;
  #   defaultEditor = true;
  #   extraPackages = with pkgs; [
  #     ripgrep # Requirement for telescope
  #     fd
  #     fzf
  #     nodejs
  #     unzip
  #
  #     rust-analyzer
  #   ];
  # };

  home.packages = with pkgs; [
    xdg-utils # wl-copy uses it
    lm_sensors
    gcc
    killall
    parted
    gparted
    gnumake
    headsetcontrol
    tldr
    neovide
    xfce.thunar
    tree
    bottom
    exa
    alacritty
    unzip
    pavucontrol
    nodePackages.pnpm
    ncspot
    discord
    slack
    libreoffice
    coreutils
    obsidian
    ncdu
    du-dust
    solaar
    (lutris.override { extraPkgs = pkgs: [ wine ]; }) #libva libva-intel-driver libva-utils ]; })
  ];

  programs.looking-glass-client.enable = true;

  programs = {
    bat.enable = true;
    zoxide.enable = true;
    lazygit.enable = true;
  };

  programs.zellij = {
    enable = true;
    #enableFishIntegration = true;
    settings = {
      pane_frames = false;
      on_force_close = "quit";
      themes = {
        catppuccin = {
          fg = "#c6c8d0";
          bg = "#626880";
          black = "#292c3c";
          red = "#e78284";
          green = "#a6d189";
          yellow = "#e5c890";
          blue = "#8caaee";
          magenta = "#f4b8e4";
          cyan = "#99d1db";
          white = "#c6c8d0";
          orange = "#ef9f76";
        };
      };
      theme = "catppuccin";
    };
  };

  programs.starship = {
    enable = true;

    settings = {
      add_newline = false;

      line_break.disabled = true;
    };
  };

  programs.git = {
    enable = true;
    userName = "eloback";
    userEmail = "eduardolloback@gmail.com";
  };

  programs.direnv = {
    enable = true;
  };

  #".config/alacritty/alacritty.yml".source = ./alacritty.yml;

  programs.firefox = {
    enable = true;
  };

  programs.chromium = {
    enable = true;
    extensions = [
      { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; } # ublock origin
      { id = "nngceckbapebfimnlniiiahkandclblb"; } # bitwarden
    ];
  };

  programs.skim = {
    enable = true;
    enableFishIntegration = true;
  };

  programs.qutebrowser = {
    enable = true;
    searchEngines = {
      nw = "https://nixos.wiki/index.php?search={}";
    };
  };
}
