{ pkgs, ... }:
{
  xdg.configFile = {
    "fish/init.fish".source = ./config.fish;
  };
  programs.fish = {
    enable = true;
    interactiveShellInit = ''
      # Setting up zoxide
      # zoxide init fish | source
    '';
    shellInit = "source ~/.config/fish/init.fish";
  };
}
