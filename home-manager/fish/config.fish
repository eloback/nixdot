# Provisory ssh-agent setup

function sshagent_findsockets
    find /tmp -uid (id -u) -type s -name agent.\* 2>/dev/null
end

function sshagent_testsocket
    if [ ! -x (command which ssh-add) ]
        echo "ssh-add is not available; agent testing aborted"
        return 1
    end

    if [ X"$argv[1]" != X ]
        set -xg SSH_AUTH_SOCK $argv[1]
    end

    if [ X"$SSH_AUTH_SOCK" = X ]
        return 2
    end

    if [ -S $SSH_AUTH_SOCK ]
        ssh-add -l >/dev/null
        if [ $status = 2 ]
            echo "Socket $SSH_AUTH_SOCK is dead!  Deleting!"
            rm -f $SSH_AUTH_SOCK
            return 4
        else
            echo "Found ssh-agent $SSH_AUTH_SOCK"
            return 0
        end
    else
        echo "$SSH_AUTH_SOCK is not a socket!"
        return 3
    end
end


function ssh_agent_init
    # ssh agent sockets can be attached to a ssh daemon process or an
    # ssh-agent process.

    set -l AGENTFOUND 0

    # Attempt to find and use the ssh-agent in the current environment
    if sshagent_testsocket
        set AGENTFOUND 1
    end

    # If there is no agent in the environment, search /tmp for
    # possible agents to reuse before starting a fresh ssh-agent
    # process.
    if [ $AGENTFOUND = 0 ]
        for agentsocket in (sshagent_findsockets)
            if [ $AGENTFOUND != 0 ]
                break
            end
            if sshagent_testsocket $agentsocket
                set AGENTFOUND 1
            end

        end
    end

    # If at this point we still haven't located an agent, it's time to
    # start a new one
    if [ $AGENTFOUND = 0 ]
        echo need to start a new agent
        eval (ssh-agent -c)
    end

    # Finally, show what keys are currently in the agent
    # ssh-add -l
end

# ssh_agent_init

# ------------------------ #
# End of provisory setup   #

set -g fish_gretting

#starship init fish | source

#fish_add_path ~/.local/bin
#fish_add_path ~/.cargo/bin

# fix st bug with helix
#set -u COLORTERM truecolor
#export COLORTERM=truecolor
#set -g fish_term24bit 1

# DEFAULT Apps
#set -Ux EDITOR nvim
#set -Ux VISUAL nvim
#set -g BROWSER firefox

## Abbreaviations
abbr -a cd --position command z
abbr -a cat --position anywhere bat
abbr -a ls --position command exa
abbr -a nc --position command ncspot
abbr -a notes --position command "$EDITOR ~/notes"
abbr -a lgit --position command lazygit
alias exa="exa -1l --group-directories-first"
# config config --local status.showUntrackedFiles no # use this to use the alias bellow
#alias config="/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME"
#alias lconfig="lazygit --git-dir=$HOME/dotfiles/ --work-tree=$HOME"

alias lvim="NVIM_APPNAME=nvimlazy nvim"
#alias nvim=lvim

# Nixos Goodies
alias nxgit="lazygit -p $HOME/flakes/system/"
alias nxcfg="$EDITOR ~/flakes/system/flake.nix --cmd 'cd %:p:h'"
alias nxr="sudo nixos-rebuild switch --flake ~/flakes/system#nixos"

function ziu -d "fzf and cd directory, also know as 'zi unknown'"
    cd "$(find -type d | fzf)"
end

# Developer abbrs
abbr -a ngw --position command "ngrok http --domain=agil.ngrok.app"
abbr -a pn --position command pnpm

alias npm="pnpm"
alias yarn="pnpm"

# Tmux goodies
# abbr -a tn --position command "tmux new -s (pwd | sed 's/.*\///g')"
# abbr -a tfs --position command "tmux attach-session -t (tmux ls | fzf | sed -E 's/:.+//g')"

# Mais Agil
abbr -a massh --position command "ssh (printf 'root\nwwmais\nmaisagil' | fzf)@162.215.223.78"

abbr -a cfg --position command "$EDITOR ~/.config/(ls ~/.config | sk)"

abbr -a cargowatch --position command "cargo watch -x check -x test -x run"

alias tm="zellij"

alias cargohelper1='cargo watch -q -c -w tests/ -x "test -q quick_dev -- --nocapture"'
alias cargohelper2='cargo watch -q -c -w src/ -x run'
abbr -a zl --position command "zellij -l ~/.config/zellij/layouts/(ls ~/.config/zellij/layouts | sk)"
