{ config, lib, pkgs, ... }:
{
#  wayland.windowManager.hyprland.enable = true;
  programs.waybar.enable = true;
  services.mako.enable = true;
  programs.wofi.enable = true;

  home.packages = with pkgs; [
    hyprpaper
    wl-clipboard
    grim
    slurp
  ];
}
