require("bufferline").setup({})

require("lualine").setup({
	options = {
		icons_enabled = true,
		theme = "catppuccin",
	},
})
