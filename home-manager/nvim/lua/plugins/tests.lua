require("neotest").setup({
  adapters = {
    require("neotest-vitest"),
  },
  status = { virtual_text = true },
    output = { open_on_run = true },
    quickfix = {
      open = function()
          vim.cmd("Trouble quickfix")
      end,
    },
})

vim.keymap.set("n", "<leader>tt", function()
  require("neotest").run.run(vim.fn.expand("%"))
end, { silent = true, noremap = true })
vim.keymap.set("n", "<leader>tT", function()
  require("neotest").run.run(vim.loop.cwd())
end, { silent = true, noremap = true })
vim.keymap.set("n", "<leader>tr", function()
  require("neotest").run.run()
end, { silent = true, noremap = true })
vim.keymap.set("n", "<leader>ts", function()
  require("neotest").summary.toggle()
end, { silent = true, noremap = true })
vim.keymap.set("n", "<leader>to", function()
  require("neotest").output.open({ enter = true, auto_close = true })
end, { silent = true, noremap = true })
vim.keymap.set("n", "<leader>tO", function()
  require("neotest").output_panel.toggle()
end, { silent = true, noremap = true })
vim.keymap.set("n", "<leader>tS", function()
  require("neotest").run.stop()
end, { silent = true, noremap = true })
