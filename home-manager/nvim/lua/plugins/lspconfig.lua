-- smart dev enviroment / lsp features for nvim folder
require("neodev").setup({
  library = { plugins = { "neotest" }, types = true },
})

local lspconfig = require("lspconfig")

local on_attach = function(_, _)
  vim.keymap.set("n", "gd", vim.lsp.buf.definition, {})
  vim.keymap.set("n", "K", vim.lsp.buf.hover, {})
  vim.keymap.set("n", "gi", vim.lsp.buf.implementation, {})
  vim.keymap.set("n", "gr", require("telescope.builtin").lsp_references, {})
  vim.keymap.set({ "n", "v" }, "<space>ca", vim.lsp.buf.code_action, {})
  vim.keymap.set("n", "<space>cf", function()
    vim.lsp.buf.format({ async = true })
  end, {})
  vim.keymap.set("n", "<space>cr", vim.lsp.buf.rename, {})
end

local capabilities = require("cmp_nvim_lsp").default_capabilities()

local defaultSetup = function()
  return {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

lspconfig.rust_analyzer.setup({
  on_attach = on_attach,
  capabilities = capabilities,
})
lspconfig.lua_ls.setup({
  on_attach = on_attach,
  capabilities = capabilities,
})
lspconfig.rnix.setup({
  on_attach = on_attach,
  capabilities = capabilities,
})

-- Typescript
lspconfig.tsserver.setup(defaultSetup())
lspconfig.eslint.setup({
  settings = {
    packageManager = "pnpm",
  },
  on_attach = function(client, bufnr)
    vim.api.nvim_create_autocmd("BufWritePre", {
      buffer = bufnr,
      command = "EslintFixAll",
    })
    on_attach(client, bufnr)
  end,
})
lspconfig.tailwindcss.setup(defaultSetup())
lspconfig.prismals.setup(defaultSetup())

-- null_ls
local null_ls = require("null-ls")

null_ls.setup({
  sources = {
    null_ls.builtins.formatting.shfmt,
    null_ls.builtins.formatting.stylua,
    null_ls.builtins.diagnostics.fish,
    null_ls.builtins.formatting.fish_indent,
  },
})
