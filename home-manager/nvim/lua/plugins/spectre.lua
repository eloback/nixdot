require('spectre').setup({})

vim.keymap.set("n", "<leader>sr", function()
  require('spectre').open()
end, {})
