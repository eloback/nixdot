require("mini.indentscope").setup({})
require("mini.comment").setup({
  options = {
    custom_commentstring = function()
      return require("ts_context_commentstring.internal").calculate_commentstring() or vim.bo.commentstring
    end,
  },
})
require("mini.pairs").setup({})


local bufremove = require("mini.bufremove")
bufremove.setup({})
vim.keymap.set("n", "<leader>bd", function()
	bufremove.delete(0, false)
end, { desc = "Delete Buffer" })
vim.keymap.set("n", "<leader>bD", function()
	bufremove.delete(0, true)
end, { desc = "Delete Buffer (Force)" })

require("illuminate").configure({
	delay = 200,
	large_file_cutoff = 2000,
	large_file_overrides = {
		providers = { "lsp" },
	},
})

