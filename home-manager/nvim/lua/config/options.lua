local opt = vim.opt
local g = vim.g

g.mapleader = " "
g.maplocalleader = " "

-- Identation
opt.smartindent = true
opt.autoindent = true
opt.backspace = "2"
opt.tabstop = 2
opt.shiftwidth = 2
opt.shiftround = true
opt.expandtab = true

opt.clipboard = "unnamedplus"
opt.mouse = "a"
opt.wrap = false

-- Ui
opt.termguicolors = true
opt.showcmd = true
opt.laststatus = 2
opt.autowrite = true
opt.cursorline = true
vim.wo.relativenumber = true
opt.smartcase = true
opt.ttimeoutlen = 300
opt.compatible = false
opt.autoread = true
