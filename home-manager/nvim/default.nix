{ pkgs, ... }:
{
  xdg.configFile = {
    "nvim/init.lua".source = ./init.lua;
    "nvim/lua".source = ./lua;
    "nvim/lua".recursive = true;
  };
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    vimAlias = true;
    plugins = with pkgs.vimPlugins; [
      # Dependencies
      plenary-nvim
      vim-repeat
      telescope-fzf-native-nvim
      nvim-ts-context-commentstring
      nvim-treesitter.withAllGrammars
      # Visual
      nvim-web-devicons
      indent-blankline-nvim
      catppuccin-nvim
      vim-illuminate
      bufferline-nvim
      dressing-nvim
      lualine-nvim
      nvim-colorizer-lua
      # Util
      vimwiki
      oil-nvim
      todo-comments-nvim
      trouble-nvim
      leap-nvim
      flit-nvim
      telescope-nvim
      which-key-nvim
      vim-closetag
      nvim-spectre
      mini-nvim
      # DAP
      neotest
      neotest-vitest
      # Lsp and cmp stuff
      trouble-nvim
      vim-nix
      neodev-nvim
      nvim-lspconfig
      null-ls-nvim
      nvim-cmp
      cmp-nvim-lsp
      luasnip
      cmp_luasnip
      copilot-lua
      copilot-cmp
      friendly-snippets
      cmp-buffer
      cmp-path
    ];
    extraPackages = with pkgs; [
      ripgrep # Requirement for telescope
      fd
      rnix-lsp
      nixfmt
      lua-language-server
      stylua
      # Web dev is a cancer.
      nodejs
      nodePackages.typescript
      nodePackages.typescript-language-server
      nodePackages."@prisma/language-server"
      nodePackages."@tailwindcss/language-server"
      nodePackages.vscode-langservers-extracted
      nodePackages.prettier_d_slim
    ];
  };
}
