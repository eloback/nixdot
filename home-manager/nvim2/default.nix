{ pkgs, ... }:
{
  xdg.configFile = {
    "nvim/init.lua".source = ./lazyvim/init.lua;
    "nvim/lua".source = ./lazyvim/lua;
    "nvim/stylua.toml".source = ./lazyvim/stylua.toml;
    "nvim/lua".recursive = true;
  };
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    vimAlias = true;
    extraPackages = with pkgs; [
      ripgrep # Requirement for telescope
      fd
    ];
  };
}
