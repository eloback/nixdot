{ config, lib, pkgs, ... }:

let 
  mod = "Mod4";
in {
  home.packages = with pkgs; [
    pulseaudio
    maim
    xclip
  ];

  xsession = {
    enable = true;

    scriptPath = ".hm-xsession";

    windowManager.i3 = {
      enable = true;

      config = {
        startup = [
          { command = "chromium"; workspace = "2"; }
          { command = "discord"; workspace = "4"; }
        ];

        defaultWorkspace = "workspace number 1";

        fonts = {
            names = [ "JetBrainsMono Nerd Font" ];
            size = 16.0;
          };

        gaps = {
            inner = 6;
            smartBorders = "on";
          };

        terminal = "alacritty";

        floating.criteria = [{ class = "Pavucontrol"; }];
        modifier = mod;
        keybindings = lib.mkOptionDefault {
          "${mod}+j" = "focus prev";
          "${mod}+k" = "focus next";

	  "${mod}+m" = "exec pavucontrol";
          "${mod}+x" = "exec sh -c '${pkgs.maim}/bin/maim -s | xclip -selection clipboard -t image/png'";
          "${mod}+Shift+x" = "exec sh -c maim -s ~/Pictures/$(date +%s).png";
          #"${mod}+Shift+x" = "exec sh -c '${pkgs.i3lock}/bin/i3lock -c 222222 & sleep 5 && xset dpms force of'";
          "${mod}+q" = "kill";
          "XF86AudioRaiseVolume" = "exec --no-startup-id pactl set-sink-volume 0 +5%"; #increase sound volume
          "XF86AudioLowerVolume" = "exec --no-startup-id pactl set-sink-volume 0 -5%"; #decrease sound volume"
          "XF86AudioMute" = "exec --no-startup-id pactl set-sink-mute 0 toggle"; # mute sound
        };

      };
    };
  };
}
