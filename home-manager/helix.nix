{ pkgs, ... }:
{
  home.packages = with pkgs; [
    nil
  ];

  programs.helix = {
    enable = true;
    settings = {
        theme = "catppuccin_frappe";
        editor = {
          line-number = "relative";
          cursorline = true;
          color-modes = true;
          bufferline = "always";
          auto-format = true;
          #shell = ["nu" "-c"]

          soft-wrap.enable = true;


          cursor-shape = {
            insert = "bar";
            select = "underline";
          };
          file-picker.hidden = false;

          statusline = {
            left = ["mode" "spinner"];
            center = ["version-control" "file-name"];
            right = ["diagnostics" "selections" "position" "file-encoding" "file-line-ending" "file-type"];
            separator = "│";
            mode.normal = "NORMAL";
            mode.insert = "INSERT";
            mode.select = "SELECT";
          };

          indent-guides.render = true;
        };

        keys.normal = {
          esc=["collapse_selection" "keep_primary_selection"];
          #C-d=["half_page_down" "align_view_center"];
          #C-u=["half_page_up" "align_view_center"];
          z.w=[":toggle soft-wrap.enable"];

          space = {
            q=":buffer-close";
            space="file_picker";
          };
        };
        keys.select = {
          z.w=[":toggle soft-wrap.enable"];
          space.q=":buffer-close";
        };
    };
  };
}
